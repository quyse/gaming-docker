#!/bin/bash

# hack for /usr/bin/unity script in 16.10 and 17.04
# setting --log avoids check for non-existent thing in python map
# yes, code is absolute shit there

# run unity first time to let it show shortcuts overlay
xinit $(which bash) -c "unity --log=log.txt & sleep 10" -- $(which Xvfb) :20 -screen 0 1280x1024x24
sleep 10
xinit $(which bash) -c "unity --log=log.txt & (sleep 10 && LIBGL_ALWAYS_SOFTWARE=1 GALLIUM_DRIVER=swr LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/runner/prefix/lib $*)" -- $(which Xvfb) :20 -screen 0 1280x1024x24
